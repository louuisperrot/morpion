/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JComponent;
import morpion.Case;

/**
 *
 * @author Admin
 */
public class PartieComponent extends JComponent{
    private Case tabCase[][];
    private int taille;
    private int tailleCase=150;
    int player=1;
    int nbCoup=0;

    
    
    public PartieComponent(int taille){
        
        this.taille=taille;
        tabCase=new Case[taille][taille];
        for(int i=0;i<taille;i++){
            for(int j=0;j<taille;j++){
                tabCase[i][j]=new Case(0,i,j);
            }
        }
        
        
    }
    
    public void tour(){
        
    }
    
    public int jouer(int x,int y){
        
        
        //System.out.println("x="+x+" y="+y);
        
        
        
        if(tabCase[x][y].getEtat()==0){
            if(player==1){
                tabCase[x][y].setEtat(1);
                
                
            }else if(player==2){
                tabCase[x][y].setEtat(2);
                
            }
        //    return false;
            //checkFin(x,y,symbole);
        }
        repaint();
        nbCoup++;
        if(nbCoup==9){
            return -2;
        }
        if(!checkFin(x, y,player)){
            if(player==1){
                player=2;
            }else{
                if(player==2)
                    player=1;
            }
                return player;
        }else{
            if(player==1){
                player=2;
            }else{
                if(player==2)
                    player=1;
            }
            return -1;
        }
        
        
        
            
        
        //System.out.println("fin repaint");
        
    }
    
    public boolean checkFin(int x,int y,int symbole){
        
        //detection sur x
        //System.out.println("symbole: "+symbole);
        //System.out.println("case1:"+tabCase[(x+1)%taille][y].getEtat()+", case2:"+tabCase[(x+2)%taille][y].getEtat());
        if(tabCase[(x+1)%taille][y].getEtat()==symbole && tabCase[(x+2)%taille][y].getEtat()==symbole){
            return false;
        }
        //detection sur y
        if(tabCase[x][(y+1)%taille].getEtat()==symbole && tabCase[x][(y+2)%taille].getEtat()==symbole){
            return false;
        }
        
        // diag haut droite-gauche
        if(x==y){
            if( tabCase[(x+1)%taille][(y+1)%taille].getEtat()==symbole && tabCase[(x+2)%taille][(y+2)%taille].getEtat()==symbole){
                return false;
            }  
        }
        
        if((x==2 && y==0)||(x==1 && y==1)||(x==0 && y==2)){
            //System.out.println("case1: "+tabCase[2][0].getEtat()+", case2: "+tabCase[1][1].getEtat()+", case2: "+tabCase[0][2].getEtat());
            if(tabCase[2][0].getEtat()==tabCase[1][1].getEtat() && tabCase[2][0].getEtat()==tabCase[0][2].getEtat()){
                return false;
            } 
        }
        
        
        return true;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        for (int i =0; i < taille; i++){
            for(int j=0; j < taille; j++){
                if (tabCase[i][j].getEtat()== 0)
                    g.setColor(Color.WHITE);
                else{
                    if (tabCase[i][j].getEtat()== 1)
                        g.setColor(Color.RED);
                    else{
                        if (tabCase[i][j].getEtat()== 2)
                        g.setColor(Color.BLUE);
                    }
                }
                
                g.fillRect(i*tailleCase, j*tailleCase, tailleCase, tailleCase);
                g.setColor(Color.BLACK);
                g.drawRect(i*tailleCase, j*tailleCase, tailleCase, tailleCase);
            }
        }
    }
    
    @Override
    public Dimension getPreferredSize() { return new Dimension(taille*tailleCase, taille*tailleCase); }
}
