package morpion;

public class Case {
    private int etat;
    private int x,y;

    public Case(int x,int y){
        etat=0;
        this.x=x;
        this.y=y;
    }
    
    public Case(int etat,int x,int y){
        this.etat=etat;
        this.x=x;
        this.y=y;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public int getEtat() {
        return etat;
    }
    
    
}
