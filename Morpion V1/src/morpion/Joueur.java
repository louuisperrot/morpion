
package morpion;

public class Joueur {
    String pseudo;
    int forme;
    
    public Joueur(String pseudo,int forme){
        this.pseudo=pseudo;
        this.forme=forme;
    }

    public String getPseudo() {
        return pseudo;
    }

    public int getForme() {
        return forme;
    }
    
    
}
